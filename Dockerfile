FROM andkram/malmo:latest

RUN sudo apt -y update && sudo apt -y upgrade
RUN sudo pip3 install --upgrade pip
RUN sudo pip3 install gym lxml numpy pillow
RUN git clone https://github.com/Microsoft/malmo.git
RUN cd malmo/Minecraft && (echo -n "malmomod.version=" && cat ../VERSION) > ./src/main/resources/version.properties
RUN sudo pip3 install -U marlo
RUN sudo python3 -c "import marlo"
RUN sudo python3 -c "from marlo import MalmoPython"

