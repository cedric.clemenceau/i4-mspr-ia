Sources : 
https://github.com/Microsoft/malmo/tree/master/MalmoEnv
https://github.com/crowdAI/marLo

## Tuto réalisé principalement sous Unix, compléter si précisions pour Windows ou incompatibilités ##
## Sinon les VM ça existe ;) ##

-------------------

## 1. Unix : ##
 
Lancer le script build.sh pour construire l'image : 

$ ./build.sh

## 1. Windows : ## 

Exécutez la commande suivante pour construire manuellement l'image : 

$ docker build -t malmo:latest .

-------------------

2. Mise en place du volume (à effectuer une seule fois après la création de l'image)

- Commenter dans docker compose les lignes de la manière suivantes : 

#volumes:
#- ./malmo/:/home/malmo/

- Lancer docker-compose.yml :

$ docker-compose up -d

- Copier le contenu du dossier malmo provenant du container dans un dossier courant : 

sudo docker cp {CONTAINER_ID}:/home/malmo/ ./

- Arreter le container :

$ docker-compose down

- Décommenter les lignes commentées précedemment


3. Lancer l'image créée via le docker-compose.yml : 

$ docker-compose up


4. Une fois finalisée, ouvrir un terminal, puis exécuter un bash dans le container malmo pour s'y connecter et rentrer des commandes :

$ docker exec -it {CONTAINER_ID} bash


## Lancer un test avec marlo ##
-------------------
5. Pour lancer un test (à un agent), lancer un client :

$ cd malmo/Minecraft

$ ./launchClient.sh -port {PORT_CLI_MINECRAFT} -env


6. Ouvrir l'interface Jupyter et entrer le token

navigateur -> localhost:8888

Répeter l'étape 4. et taper la commande :

$ jupyter notebook list 

Copier coller le token dans le formulaire


7. Créer un nouveau notebook dans Jupyter en Python 3, y mettre du code (code d'exemple dans https://github.com/crowdAI/marLo) avec le bon {PORT_CLI_MINECRAFT} au bon endroit dans le code


8. Cliquer sur Run


## Lancer un test avec malmo ##
-------------------
5. Pour lancer un test (à un agent), lancer un client :

$ cd malmo/Minecraft

$ ./launchClient.sh -port {PORT_CLI_MINECRAFT} -env


6. Puis lancer le test voulu dans un autre terminal (répéter l'étape 4. pour ouvrir un nouveau terminal dans malmo): 

$ cd malmo/MalmoEnv

$ python3 run.py --mission missions/mobchase_single_agent.xml --port {PORT_CLI_MINECRAFT} --episodes 10
